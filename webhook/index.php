<?php

$json = json_decode(file_get_contents('php://input'));

$msg = "";

if ($json->live->is_live)
    $msg .= "({$json->live->streamer_name}) ";
$msg .= "now playing: ";
$msg .= $json->now_playing->song->text . " ";
$msg .= "~~ {$json->listeners->current} listeners";

file_put_contents(__DIR__."/../now_playing.txt", $msg);

