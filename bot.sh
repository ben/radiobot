#!/bin/bash

. bot.properties
input=".bot.cfg"
printf "Starting session: %s\n" "$(date "+[%y:%m:%d %T]")" > $log

# connect and register
printf "NICK %s\r\n" "$nick" > $input
printf "USER %s 0 * :%s\r\n" "$nick" "$nick" >> $input
printf "MODE %s +B\r\n" "$nick" >> $input


function parse_dj {
    grep -Eo '^\([^)]*\)' $npfile | sed 's/[()]//g' | xargs
}

function msg {
    printf "PRIVMSG %s :%s\r\n" "$1" "$2" >> $input
}

# save current dj info on startup
now_playing=$(<$npfile)
dj=$(parse_dj)

# main loop
tail -f $input | telnet $server $port | while read -r buf
do
    # trim \r's
    line=$(printf "%b" "$buf" | tr -d $'\r')
    printf "%s\n" "$line"

    # log the session
    printf "%s: %s\n" "$(date "+[%y:%m:%d %T]")" "$line" >> $log

    # now playing
    if [[ $now_playing != "$(<$npfile)" ]]; then
        now_playing=$(<$npfile)
        [[ ! -z "${dj}" ]] && \
            printf "PRIVMSG #%s :\x0303%s\r\n" "$channel" "$now_playing" >> $input
    fi

    # new dj!
    if [[ $dj != $(parse_dj) ]]; then
        dj=$(parse_dj)
        if [[ ! -z "${dj}" ]]; then
            for chan in $notify_channels; do
                msg "#$chan" "$dj is now online playing $now_playing! tune in now here: $link"
            done

            while IFS= read -r -u 10 subscriber; do
                msg $subscriber "$dj is now online playing $now_playing! tune in now here: $link"
            done 10< subscribers.txt

            while IFS= read -r -u 11 subscriber email_addr; do
                sed -e "s/<dj>/$dj/g" \
                    -e "s|<link>|$link|g" \
                    -e "s/<now_playing>/$now_playing/g" \
                    -e "s/<email_addr>/$email_addr/g" dj-online-email.tmpl \
                    | sendmail $email_addr
            done 11< email-subscribers.txt

            for json in ./*-toot.json; do
                toot --creds $json "dj $dj is now streaming live on https://tilderadio.org! tune in here: $link"
            done
        fi
    fi

    # do things when you see output
    case "$line" in
        # respond to ping requests from the server
        PING*)
            printf "%s\r\n" "$line" | sed "s/I/O/" >> $input
            ;;

        # make sure we're joined
        *376*|*404*)
            for chan in $notify_channels; do
                printf "JOIN #%s\r\n" "$chan" >> $input
            done
            ;;

        # run when a message is seen
        *PRIVMSG*)
            who=$(printf "%s" "$line" | sed -E "s/:(.*)\!.*@.*/\1/")
            from=$(printf "%s" "$line" | sed -E "s/.*PRIVMSG (.*[#]?([a-zA-Z]|\-)*) :.*/\1/")

            # "#" would mean it's a channel
            if [[ $from =~ ^#.* ]]; then
                test "$(printf "%s" "$line" | grep ":$nick:")" || continue
                args=$(printf "%s" "$line" | sed -E "s/.*:$nick:(.*)/\1/")
            else
                args=$(printf "%s" "$line" | sed -E "s/.*$nick :(.*)/\1/")
                from="$who"
            fi

            # trim leading and split args on space
            args=( $(printf "%s" "$args" | sed -E "s/^ //") )

            case "${args[0]}" in
                subscribe)
                    if grep -q "$who" subscribers.txt; then
                        msg $from "you're already subscribed! :)"
                    else
                        printf "%s\n" "$who" >> subscribers.txt
                        msg $from "i'll send you a direct message when a dj starts streaming!"
                    fi
                    ;;

                unsubscribe)
                    sed -i "/$who/d" subscribers.txt
                    msg $from "i'll stop sending you updates."
                    ;;

                email-subscribe)
                    if grep -q $who email-subscribers.txt; then
                        msg $from "you're already subscribed! :)"
                    else
                        if (( "${#args[1]}" > 0 )); then
                            printf "%s %s\n" "$who" "${args[1]}" >> email-subscribers.txt
                            msg $from "i'll send you an email when a dj starts streaming!"
                        else
                            msg $from "please provide an address to email when a dj starts streaming"
                        fi
                    fi
                    ;;

                email-unsubscribe)
                    sed -i "/$who/d" email-subscribers.txt
                    msg $from "i'll stop sending you email updates."
                    ;;

                time)
                    msg $from "$(TZ=UTC date)"
                    ;;

                np)
                    msg $from "$now_playing"
                    ;;

                dj)
                    msg $from "${dj:-"no one"} is on the air now"
                    ;;

                link)
                    msg $from "$link"
                    ;;

                source)
                    msg $from "$source"
                    ;;

                schedule)
                    msg $from "$schedule"
                    ;;

                help)
                    msg $from "hey hi, my commands are np, dj, link, slogan, schedule, [un]subscribe, email-[un]subscribe. please provide an address when subscribing via email."
                    ;;

                radio|slogan)
                    msg $from "$(shuf -n1 slogans.txt)"
                    ;;

                paymybills)
                    msg $from "whaddya mean?! i'm broker than you!"
                    ;;

                *) ;;

            esac
            ;;

        # else
        *) ;;
    esac
done

